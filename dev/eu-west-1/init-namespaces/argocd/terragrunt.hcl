include "root" {
  path           = find_in_parent_folders()
  expose         = true
  merge_strategy = "deep"
}


terraform {
  source = "git::https://github.com/gruntwork-io/terraform-kubernetes-namespace.git//modules/namespace?ref=v0.5.0"
}

dependency "eks" {
  config_path = "../../eks"
  mock_outputs = {
    oidc_provider_arn = "arn:"
    cluster_id = "826319"
    cluster_oidc_issuer_url = "https://oidc.cluster"
  }
}

generate "provider-local" {
  path      = "provider-local.tf"
  if_exists = "overwrite"
  contents  = file("../../../../provider-config/helm/helm.tf")
}

generate "cluster-var" {
  path      = "cluster-var.tf"
  if_exists = "overwrite"
  contents  = <<EOF
variable "cluster-name" {
  description = "Name of the Kubernetes cluster"
  default     = "sample-cluster"
  type        = string
}
EOF
}

inputs = {

  cluster-name = dependency.eks.outputs.cluster_id
  name = "argocd"

}
