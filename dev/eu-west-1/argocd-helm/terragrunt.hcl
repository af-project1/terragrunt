include "root" {
  path           = find_in_parent_folders()
  expose         = true
  merge_strategy = "deep"
}


terraform {
  source = "tfr:///terraform-module/release/helm?version=2.7.0"
}

dependency "eks" {
  config_path = "../eks"
  mock_outputs = {
    oidc_provider_arn = "arn:"
    cluster_id = "826319"
    cluster_oidc_issuer_url = "https://oidc.cluster"
  }
}

dependency "namespace" {
  config_path = "../init-namespaces/argocd"
}

generate "provider-local" {
  path      = "provider-local.tf"
  if_exists = "overwrite"
  contents  = file("../../../provider-config/helm/helm.tf")
}

generate "cluster-var" {
  path      = "cluster-var.tf"
  if_exists = "overwrite"
  contents  = <<EOF
variable "cluster-name" {
  description = "Name of the Kubernetes cluster"
  default     = "sample-cluster"
  type        = string
}
EOF
}

inputs = {

  cluster-name = dependency.eks.outputs.cluster_id
  namespace  = "argocd"
  repository =  "https://argoproj.github.io/argo-helm"
  app = {
    name          = "argocd"
    version       = "4.9.4"
    chart         = "argo-cd"
    force_update  = true
    wait          = false
    recreate_pods = false
    deploy        = 1
  }
  values = [file("values.yaml")]

}
