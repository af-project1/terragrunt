include "root" {
  path           = find_in_parent_folders()
  expose         = true
  merge_strategy = "deep"
}

terraform {
  source = "tfr:///terraform-aws-modules/eks/aws?version=18.20.5"
#  after_hook "kubeconfig" {
#    commands = ["apply"]
#    execute  = ["bash", "-c", "aws eks update-kubeconfig --name ${include.root.locals.full_name} --kubeconfig  ${get_terragrunt_dir()}/kubeconfig 2>/dev/null"]
#  }
}

dependency "vpc" {
  config_path = "../vpc"
  mock_outputs = {
    vpc_id = "vpc-00000000"
    private_subnets = [
      "subnet-00000000",
      "subnet-00000001",
      "subnet-00000002",
    ]
    public_subnets = [
      "subnet-00000003",
      "subnet-00000004",
      "subnet-00000005",
    ]
    private_subnets_cidr_blocks = [
      "192.168.0.0/16",
      "10.0.0.0/8",
      "172.16.0.0/12"
    ]
    intra_route_table_ids     = []
    private_route_table_ids   = []
    public_route_table_ids    = []
    default_security_group_id = "sg-00000000"
  }
}

generate "provider-local" {
  path      = "provider-local.tf"
  if_exists = "overwrite"
  contents  = file("../../../provider-config/eks/eks.tf")
}

inputs = {

  cluster_name                = include.root.locals.full_name
  cluster_version             = "1.22"
  vpc_id = dependency.vpc.outputs.vpc_id
  subnet_ids                     = dependency.vpc.outputs.private_subnets
  cloudwatch_log_group_retention_in_days = 7

  tags = merge(
    include.root.locals.custom_tags
  )

  node_security_group_additional_rules = {
    ingress_self_all = {
      from_port = 0
      to_port   = 0
      protocol  = "-1"
      type      = "ingress"
      self      = true
    }
    ingress_cluster_all = {
      from_port                     = 0
      to_port                       = 0
      protocol                      = "-1"
      type                          = "ingress"
      source_cluster_security_group = true
    }
    ingress_node_port_tcp = {
      from_port        = 30000
      to_port          = 32767
      protocol         = "tcp"
      type             = "ingress"
      cidr_blocks      = ["0.0.0.0/0"]
      ipv6_cidr_blocks = ["::/0"]
    }
    ingress_node_port_udp = {
      from_port        = 30000
      to_port          = 32767
      protocol         = "udp"
      type             = "ingress"
      cidr_blocks      = ["0.0.0.0/0"]
      ipv6_cidr_blocks = ["::/0"]
    }
    egress_all = {
      from_port        = 0
      to_port          = 0
      protocol         = "-1"
      type             = "egress"
      cidr_blocks      = ["0.0.0.0/0"]
      ipv6_cidr_blocks = ["::/0"]
    }
  }

  eks_managed_node_group_defaults = {
    disk_size      = 50
    root_volume_type = "gp2"
  }

  eks_managed_node_groups = {
    test = {
      min_size     = 3
      max_size     = 5
      desired_size = 3

      instance_types = ["t3.small"]
      capacity_type  = "SPOT"
      pre_bootstrap_user_data = <<-EOT
        #!/bin/bash
        set -ex
        cat <<-EOF > /etc/profile.d/bootstrap.sh
        export CONTAINER_RUNTIME="containerd"
        export USE_MAX_PODS=false
        EOF
        # Source extra environment variables in bootstrap script
        sed -i '/^set -o errexit/a\\nsource /etc/profile.d/bootstrap.sh' /etc/eks/bootstrap.sh
        EOT
    }
  }
  manage_aws_auth_configmap = true
  aws_auth_users = [
    {
      userarn  = "arn:aws:iam::563133610945:user/vtitov"
      username = "vtitov"
      groups   = ["system:masters"]
    },
  ]
}
