# Generate backend.tf file with remote state configuration
remote_state {
  backend = "s3"
  generate = {
    path      = "_backend.tf"
    if_exists = "overwrite"
  }

  config = {
    bucket  = "${yamldecode(file(find_in_parent_folders("global_values.yaml")))["prefix"]}-${yamldecode(file(find_in_parent_folders("env_values.yaml")))["env"]}-tg-state-store"
    region  = "${yamldecode(file(find_in_parent_folders("global_values.yaml")))["tf_state_bucket_region"]}"
    key     = "${path_relative_to_include()}/terraform.tfstate"
    encrypt = true
  }
}

locals {
  merged = merge(
    yamldecode(file(find_in_parent_folders("global_values.yaml"))),
    yamldecode(file(find_in_parent_folders("env_values.yaml"))),
    yamldecode(file(find_in_parent_folders("region_values.yaml"))),
    yamldecode(file(find_in_parent_folders("component_values.yaml")))
  )
  custom_tags = merge(
    yamldecode(file(find_in_parent_folders("global_tags.yaml"))),
    yamldecode(file(find_in_parent_folders("env_tags.yaml"))),
    yamldecode(file(find_in_parent_folders("region_tags.yaml"))),
    yamldecode(file(find_in_parent_folders("component_tags.yaml")))
  )
  full_name = "${local.merged.prefix}-${local.merged.env}-${local.merged.name}"
  environment = "${local.merged.env}"
}

# Generate config.tf file with provider configuration
generate "my_config" {
  path      = "_config.tf"
  if_exists = "overwrite"

  contents = <<EOF
provider "aws" {
  region  = var.aws_region
  assume_role {
    role_arn = var.iam_role
  }
}
variable "aws_region" {}
variable "iam_role" {}

EOF
}

# Load Variables
terraform {
  extra_arguments "common_vars" {
    commands = get_terraform_commands_that_need_vars()

    required_var_files = [
      find_in_parent_folders("common.tfvars"),
    ]
  }
}
